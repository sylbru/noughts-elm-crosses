module Tests exposing (..)

import Expect
import Main exposing (..)
import Test exposing (..)


all : Test
all =
    describe "checkVictory"
        [ test "empty board" <|
            \_ ->
                emptyBoard
                    |> checkVictory
                    |> Expect.equal Nothing
        , test "O has first row" <|
            \_ ->
                boardFromString "ooo|xx |oxx"
                    |> checkVictory
                    |> Expect.equal (Just PlayerO)
        , test "X has first row" <|
            \_ ->
                boardFromString "xxx|oo |xoo"
                    |> checkVictory
                    |> Expect.equal (Just PlayerX)
        , test "X has second row" <|
            \_ ->
                boardFromString "oo |xxx|xoo"
                    |> checkVictory
                    |> Expect.equal (Just PlayerX)
        , test "O has second column" <|
            \_ ->
                boardFromString "oox| o |xox"
                    |> checkVictory
                    |> Expect.equal (Just PlayerO)
        , test "X has third row" <|
            \_ ->
                boardFromString " o | oo|xxx"
                    |> checkVictory
                    |> Expect.equal (Just PlayerX)
        , test "O has NW-SE diagonal" <|
            \_ ->
                boardFromString "oxx|xox|xxo"
                    |> checkVictory
                    |> Expect.equal (Just PlayerO)
        , test "X has SW-NE diagonal" <|
            \_ ->
                boardFromString "oox|oxo|xoo"
                    |> checkVictory
                    |> Expect.equal (Just PlayerX)
        , test "draw" <|
            \_ ->
                boardFromString "xox|oox|xxo"
                    |> checkVictory
                    |> Expect.equal Nothing
        , test "random Os" <|
            \_ ->
                boardFromString " o|o  | oo"
                    |> checkVictory
                    |> Expect.equal Nothing
        , test "random Xs" <|
            \_ ->
                boardFromString "xx |x x|   "
                    |> checkVictory
                    |> Expect.equal Nothing
        , test "empty lines" <|
            \_ ->
                boardFromString "   |xxx|ooo"
                    |> checkVictory
                    |> Expect.equal (Just PlayerX)
        ]


boardFromList : List Square -> Board
boardFromList list =
    case list of
        [ a0, a1, a2, b0, b1, b2, c0, c1, c2 ] ->
            Board a0 a1 a2 b0 b1 b2 c0 c1 c2

        _ ->
            emptyBoard


boardFromString : String -> Board
boardFromString string =
    string
        |> String.filter (\char -> char /= '|')
        |> String.split ""
        |> List.map squareFromString
        |> boardFromList


squareFromString value =
    case value of
        "x" ->
            Just X

        "o" ->
            Just O

        _ ->
            Nothing
