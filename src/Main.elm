module Main exposing (..)

import Browser
import Html exposing (Html, div, h1, img, text)
import Html.Attributes as Attr exposing (class, classList, src)
import Html.Events exposing (onClick)
import Svg exposing (Svg)
import Svg.Attributes as SvgAttr



---- MODEL ----


type alias Model =
    { board : Board
    , state : State
    }


type State
    = Playing Player
    | Won Player
    | Draw


type alias Board =
    { a0 : Square
    , a1 : Square
    , a2 : Square
    , b0 : Square
    , b1 : Square
    , b2 : Square
    , c0 : Square
    , c1 : Square
    , c2 : Square
    }


type Player
    = PlayerX
    | PlayerO


type Mark
    = X
    | O


type alias Square =
    Maybe Mark


init : ( Model, Cmd Msg )
init =
    ( { state = Playing PlayerX
      , board = emptyBoard
      }
    , Cmd.none
    )


emptyBoard : Board
emptyBoard =
    Board Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing



---- UPDATE ----


type Msg
    = PlaceMark Int
    | NewGame


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        PlaceMark index ->
            case model.state of
                Playing currentPlayer ->
                    ( executeMove index currentPlayer model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        NewGame ->
            init


executeMove : Int -> Player -> Model -> Model
executeMove index currentPlayer model =
    let
        newModel =
            { model | board = addMark (markFromPlayer currentPlayer) index model.board }
    in
    -- It’s imperative to test for victory BEFORE testing for draw,
    -- or we might miss victories that happen when the board is full
    case checkVictory newModel.board of
        Just winner ->
            { newModel | state = Won winner }

        Nothing ->
            if isBoardFull newModel.board then
                { newModel | state = Draw }

            else
                { newModel | state = nextPlayer model.state }


isBoardFull : Board -> Bool
isBoardFull { a0, a1, a2, b0, b1, b2, c0, c1, c2 } =
    [ a0, a1, a2, b0, b1, b2, c0, c1, c2 ]
        |> List.all (\square -> square /= Nothing)


checkVictory : Board -> Maybe Player
checkVictory board =
    let
        winningLines =
            List.filter isWinningLine (allLines board)
    in
    case winningLines of
        winningLine :: _ ->
            case winningLine of
                square :: _ ->
                    Maybe.map playerFromMark square

                _ ->
                    Nothing

        _ ->
            Nothing


allLines : Board -> List (List Square)
allLines { a0, a1, a2, b0, b1, b2, c0, c1, c2 } =
    let
        rows =
            [ [ a0, a1, a2 ]
            , [ b0, b1, b2 ]
            , [ c0, c1, c2 ]
            ]

        columns =
            [ [ a0, b0, c0 ]
            , [ a1, b1, c1 ]
            , [ a2, b2, c2 ]
            ]

        diagonals =
            [ [ a0, b1, c2 ]
            , [ c0, b1, a2 ]
            ]
    in
    rows ++ columns ++ diagonals


isWinningLine : List Square -> Bool
isWinningLine line =
    case line of
        (Just mark) :: tail ->
            List.all (\square -> square == Just mark) tail

        _ ->
            False


markFromPlayer : Player -> Mark
markFromPlayer player =
    case player of
        PlayerX ->
            X

        PlayerO ->
            O


playerFromMark : Mark -> Player
playerFromMark mark =
    case mark of
        X ->
            PlayerX

        O ->
            PlayerO


addMark : Mark -> Int -> Board -> Board
addMark mark index board =
    case index of
        0 ->
            { board | a0 = Just mark }

        1 ->
            { board | a1 = Just mark }

        2 ->
            { board | a2 = Just mark }

        3 ->
            { board | b0 = Just mark }

        4 ->
            { board | b1 = Just mark }

        5 ->
            { board | b2 = Just mark }

        6 ->
            { board | c0 = Just mark }

        7 ->
            { board | c1 = Just mark }

        8 ->
            { board | c2 = Just mark }

        _ ->
            board


nextPlayer : State -> State
nextPlayer state =
    case state of
        Playing currentPlayer ->
            case currentPlayer of
                PlayerX ->
                    Playing PlayerO

                PlayerO ->
                    Playing PlayerX

        _ ->
            state



---- VIEW ----


view : Model -> Html Msg
view model =
    div []
        [ viewBoard model.board
        , viewState model.state
        ]


viewBoard : Board -> Html Msg
viewBoard { a0, a1, a2, b0, b1, b2, c0, c1, c2 } =
    div [ class "board" ]
        (List.indexedMap
            viewSquare
            [ a0, a1, a2, b0, b1, b2, c0, c1, c2 ]
        )


viewSquare : Int -> Square -> Html Msg
viewSquare index square =
    case square of
        Just mark ->
            div [ class "board__square" ]
                [ viewMark mark ]

        Nothing ->
            div
                [ class "board__square"
                , class "board__square--available"
                , onClick (PlaceMark index)
                ]
                []


viewMark : Mark -> Svg Msg
viewMark mark =
    Svg.svg
        [ SvgAttr.width "95%"
        , SvgAttr.height "95%"
        , SvgAttr.viewBox "0 0 10 10"
        , SvgAttr.stroke "currentColor"
        , SvgAttr.strokeWidth ".5"
        ]
        [ case mark of
            X ->
                viewX

            O ->
                viewO
        ]


viewX : Svg Msg
viewX =
    Svg.path
        [ SvgAttr.d "M 1,1 L 9,9 M 1,9 L 9,1"
        , SvgAttr.strokeLinecap "round"
        ]
        []


viewO : Svg Msg
viewO =
    Svg.circle
        [ SvgAttr.cx "5"
        , SvgAttr.cy "5"
        , SvgAttr.r "4"
        , SvgAttr.fill "none"
        ]
        []


viewState : State -> Html Msg
viewState state =
    let
        statusText =
            case state of
                Playing player ->
                    playerToString player ++ " is playing…"

                Won player ->
                    playerToString player ++ " won!"

                Draw ->
                    "It’s a draw!"
    in
    Html.p [ class "status" ]
        [ text statusText
        , Html.br [] []
        , Html.button [ onClick NewGame ] [ text "New game" ]
        ]


playerToString : Player -> String
playerToString player =
    case player of
        PlayerX ->
            "Player X"

        PlayerO ->
            "Player O"



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = always Sub.none
        }
